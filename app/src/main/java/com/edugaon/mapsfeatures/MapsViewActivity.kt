package com.edugaon.mapsfeatures

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.maps.MapView

class MapsViewActivity : AppCompatActivity() {
    private var mapView:MapView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mapbox.getInstance(this, R.string.access_key.toString())
        setContentView(R.layout.activity_maps_view)
        mapView = findViewById(R.id.mapView)
        mapView?.onCreate(savedInstanceState)
        mapView?.setStyleUrl(R.string.mapbox_style_mapbox_streets.toString())

    }

    //    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
//    @SuppressLint("MissingPermission", "VisibleForTests")
//    private fun myLocation(){
//        fusedLocationClient = FusedLocationProviderClient(this)
//        fusedLocationClient!!.lastLocation.addOnSuccessListener { location ->
//           if (location != null){
//               setCameraPosition(location)
//               if (!locationSet){
//                   locationMarker = mapBox.addMarker(
//                           MarkerOptions().position(
//                                   LatLng(
//                                           location.latitude,
//                                           location.longitude
//                                   )
//                           )
//                   )
//               }else{
//                   animateMarker(location)
//               }
//           }else{
//               Toast.makeText(this, "Can't determine last location", Toast.LENGTH_LONG).show()
//           }
//
//       }
//        mapBox.uiSettings.isZoomControlsEnabled = true
//    }
//
//    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
//    private fun animateMarker(location: Location) {
//        val markerAnimator: ValueAnimator = ObjectAnimator.ofObject(
//                locationMarker!!,
//                "position",
//                LatLngEvaluator(),
//                locationMarker?.position,
//                location
//        )
//        markerAnimator.duration = 200
//        markerAnimator.start()
//    }
//
//    private fun setCameraPosition(location: Location) {
//        mapBox.animateCamera(
//                CameraUpdateFactory.newLatLngZoom(
//                        LatLng(
//                                location.latitude,
//                                location.longitude
//                        ), 14.0
//                )
//        )
//    }
//
//    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
//    override fun onRequestPermissionsResult(
//            requestCode: Int,
//            permissions: Array<out String>,
//            grantResults: IntArray
//    ) {
//        when (requestCode) {
//            PERMISSION_CODE -> {
//                if (grantResults.isNotEmpty()
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
//                ) {
//                    myLocation()
//                } else {
//
//                    // permission denied, boo!
//                    Toast.makeText(
//                            this,
//                            "Could not get location without permission",
//                            Toast.LENGTH_SHORT
//                    ).show()
//                }
//            }
//        }
//    }
//
//
//}
//
//
//class LatLngEvaluator : TypeEvaluator<LatLng> {
//    private val latLng = LatLng()
//    override fun evaluate(fraction: Float, startValue: LatLng?, endValue: LatLng?): LatLng {
//        latLng.latitude = startValue?.latitude!! +(endValue?.latitude!! - startValue.latitude * fraction)
//        latLng.longitude = startValue.longitude +(endValue.longitude - startValue.longitude * fraction)
//        return latLng
//    }


}